from django import forms
from cryptoApp.models import AssetDatePrice
from datetime import datetime as dt
from datetime import timedelta


class AssetForm(forms.Form):
    start_date = forms.DateField(initial=dt.today()+timedelta(days=-30), widget=forms.SelectDateWidget(attrs={'class':'date-entry'},years=range(2013,2019)))
    end_date = forms.DateField(initial=dt.today(), widget=forms.SelectDateWidget(attrs={'class':'date-entry'},years=range(2013,2019)))

    assets = forms.MultipleChoiceField(
        choices = [('btc', 'btc'),('bch','bch'),('ltc','ltc'),('eth','eth'),('etc','etc')],
        widget = forms.CheckboxSelectMultiple(attrs={'class':'checkbox'}),
        initial = ['btc'],
    )
