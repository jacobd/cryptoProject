from django.db import models

# Create your models here.
class AssetDatePrice(models.Model):
    assetTicker = models.CharField(max_length=4)
    date = models.DateField(db_index=True)
    price = models.FloatField(null=True)

    def __str__(self):
        return self.date,self.date,self.price


# Test these models

"""
class AssetPrice(models.Model):
    assetTicker = models.CharField(max_length=4)
    price = models.FloatField()

    def __str__(self):
        return self.assetTicker

class DatePrice(models.Model):
    asset = models.ForeignKey(AssetPrice, on_delete=models.CASCADE)
    date = models.DateField()
"""

"""
class Asset(models.Model):
    assetTicker = models.CharField(max_length=4)

    def __str__(self):
        return self.assetTicker

class PriceHistory(models.Model):
    asset = models.ForeignKey(Asset, on_delete=models.CASCADE)
    date = models.DateField()
    price = models.FloatField(default=0)

    def __str__(self):
        return self.asset, self.date, self.price,
"""
