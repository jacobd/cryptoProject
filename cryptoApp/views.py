from django.shortcuts import render
from django.http import HttpResponse
from cryptoApp.models import AssetDatePrice
from cryptoApp.forms import AssetForm
from coinMetricsAPI import formatAssetData
from coinMetricsAPI import queryAssetDatePrice
from collections import OrderedDict
import datetime


# Create your views here.

def index(request):
    form = AssetForm()
    context_dict = {}

    if request.method == 'POST':
        form = AssetForm(request.POST)
        if form.is_valid():
            # Get data from form
            data = request.POST

            # Get dates from form data
            start = datetime.datetime(int(data['start_date_year']),int(data['start_date_month']),int(data['start_date_day'])).date()
            end = datetime.datetime(int(data['end_date_year']),int(data['end_date_month']),int(data['end_date_day'])).date()

            # Get assets from form
            assets = data.getlist('assets')

            # Create context based on selected assets and dates
            context_dict = {'asset_prices':formatAssetData(queryAssetDatePrice(assets, start, end)), 'assets':assets}
        else:
            print('Form is invalid')
    else:
        assets = ['btc']
        end = datetime.datetime.today()
        start = datetime.datetime.today()+datetime.timedelta(days=-30)
        context_dict = {'asset_prices':formatAssetData(queryAssetDatePrice(assets, start, end)), 'assets':assets}

    return render(request, 'cryptoApp/index.html', {'form':form,'context':context_dict})
