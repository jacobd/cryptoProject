from django.urls import path
from cryptoApp import views

urlpatterns = [
    path('', views.index, name='index'),
]
