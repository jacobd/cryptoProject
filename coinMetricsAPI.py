import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE','cryptoProject.settings')

import django
django.setup()

import requests
import datetime
from cryptoApp.models import AssetDatePrice
import pandas as pd


def main():
    assets = ['btc','bch','ltc','eth','etc']
    dataTypes = ['price(usd)']
    start_date = datetime.datetime.today() + datetime.timedelta(days=-31)
    end_date = datetime.datetime.today()
    data = get_asset_data_for_time_range(assets, dataTypes, start_date, end_date)
    add_assetdateprice(assets,dataTypes,data)
    print(data)
    #https://coinmetrics.io/api/v1/get_asset_data_for_time_range/etc/price(usd)/1534207924/1534380724
def seconds_to_date(seconds):
    return datetime.datetime.utcfromtimestamp(seconds).date()
def date_to_seconds(date):
    return date.strftime('%s')

def get_asset_data_for_time_range(assets, data, start=datetime.date(2000,1,1), end=datetime.datetime.today()):
    coinmetrics = 'https://coinmetrics.io/api/v1/'
    endpoint = 'get_asset_data_for_time_range/'

    start = date_to_seconds(start)
    end = date_to_seconds(end)

    json_data = {}
    for asset in assets:
        json_data[asset] = {}
        for datum in data:
            json_data[asset][datum] = {}
            url = coinmetrics + endpoint + asset + '/' + datum + '/' + start + '/' + end
            json_data[asset][datum] = requests.get(url).json()
    print(json_data)
    return json_data

def add_assetdateprice(assets, data, json_data):
    for asset in assets:
        for datum in data:
            for row in json_data[asset][datum]['result']:
                AssetDatePrice.objects.get_or_create(assetTicker=asset, price=row[1], date=seconds_to_date(row[0]))

def queryAssetDatePrice(assets, start, end):
    data = {}
    # Retrieve data from database
    for asset in assets:
        data[asset] = (list(AssetDatePrice.objects.all().filter(date__gte=start).filter(date__lte=end).filter(assetTicker=asset).values()))
    return data

def formatAssetData(data):
    date_dict = {}

    for key in data:
        for item in data[key]:
            if item['date'] in date_dict:
                date_dict[item['date']].update({key:item['price']})
            else:
                date_dict[item['date']] = {}
                date_dict[item['date']] = {key:item['price']}

    df = pd.DataFrame.from_dict(date_dict, orient='index')
    df = df.fillna(method='bfill')
    df = df.fillna(method='pad')

    date_dict = df.to_dict('index')
    date_dict = sorted(date_dict.items(), reverse=True)

    return date_dict



if __name__ == '__main__':
    main()
