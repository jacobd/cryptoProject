import _mysql
from cryptoProject.settings import DATABASES
import coinMetricsAPI as api
from datetime import datetime as dt
from datetime import timedelta

assets = ['btc','bch','ltc','eth','etc']
dataTypes = ['price(usd)']

defaultDB = DATABASES['default']

print('Password:',defaultDB['PASSWORD'])

db = _mysql.connect(user=defaultDB['USER'], passwd=defaultDB['PASSWORD'])
db.query("CREATE DATABASE IF NOT EXISTS " + defaultDB['NAME'])
db.select_db(defaultDB['NAME'])

#table = db.query("SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = '{table}'".format(table='cryptoApp_assetdateprice'))
db.query("SHOW TABLES LIKE '{table}';".format(table='cryptoApp_assetdateprice'))
table = db.store_result()
table = table.fetch_row()

if table:
    db.query("SELECT * from {table} WHERE price=-1".format(table='cryptoApp_assetdateprice'))
    data = db.store_result().fetch_row()
    for row in data:
        start = dt.strptime(row[2],"%Y-%m-%d").date()
        end = dt.strptime(row[2],"%Y-%m-%d").date()
        asset = [row[1].decode("utf-8")]
        print(asset)
        data = api.get_asset_data_for_time_range(asset, dataTypes, start, end)
        api.add_assetdateprice(asset, dataTypes, data, start, end)

    for asset in assets:
        asset = [asset]
        # Data is up to date, do not need to pull API
        db.query("SELECT * from {table} WHERE assetTicker='{asset}' ORDER BY date DESC LIMIT 1".format(table='cryptoApp_assetdateprice', asset=asset[0]))
        lastRow = db.store_result().fetch_row()

        db.query("SELECT * from {table} WHERE assetTicker='{asset}' ORDER BY date ASC LIMIT 1".format(table='cryptoApp_assetdateprice', asset=asset[0]))
        firstRow = db.store_result().fetch_row()

        if lastRow:
            lastRow = dt.strptime(lastRow[0][2], "%Y-%m-%d")
            firstRow = dt.strptime(firstRow[0][2], "%Y-%m-%d")
            yesterday = dt.today() + timedelta(days=-1)

            if yesterday.weekday() > 4:
                if lastrow - yesterday > timedelta(days=-3):
                    data = api.get_asset_data_for_time_range(asset, dataTypes, lastRow+timedelta(days=1), yesterday)
                    api.add_assetdateprice(asset, dataTypes, data, lastRow+timedelta(days=1), yesterday)
                    print('Added data from {last} to {yesterday}'.format(last=lastRow,yesterday=yesterday))
                else:
                    print('No new data.')
            elif yesterday - lastRow > timedelta(days=2):
                data = api.get_asset_data_for_time_range(asset, dataTypes, lastRow+timedelta(days=1), yesterday)
                api.add_assetdateprice(asset, dataTypes, data, lastRow+timedelta(days=1), yesterday)
                print('Added new data for ',asset[0])
            elif lastRow >= yesterday:
                print('No new data.')
        else:
            data = api.get_asset_data_for_time_range(asset, dataTypes)
            api.add_assetdateprice(asset, dataTypes, data)
            print('Added all past data.')
else:
    print("No table named: {table}".format(table='cryptoApp_assetdateprice'))
    print("Please, make migrations.")

db.close()
