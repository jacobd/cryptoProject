Project: cryptoProject

About: Created an app to pull date from CoinMetrics.io, store the data in a MySQL database, to be displayed on a frontend created using Django


Prerequisites:

Python >= 3.6.5

Pip

Homebrew - Mac OS X

conda >= 4.5.9

MySQL: https://dev.mysql.com/doc/refman/8.0/en/installing.html

Installation: 

    To create a local development environment: 
    
        conda create --name “Environment name” django 
        
    To activate local development environment:
    
        conda activate “Environment name”
        
    Add user to mysql by logging in as root. **Feel free to change the Username & Password in the settings.py file**
    
        mysql -u root -p
        
        CREATE USER 'cryptoProjectUser'@'localhost' IDENTIFIED BY 'cryptoProjectPassword';
        
        GRANT ALL PRIVILEGES ON *.* to 'cryptoProjectUser'@'localhost';
        
        FLUSH PRIVILEGES;
        
    Install MySQL through Homebrew on Mac **Need to look into alternatives for Windows, etc.**
    
        brew install mysql
        
    To clone repository: **You may need to create a GitLab account and add ssh key**
    
        git clone git@gitlab.com:jacobd/cryptoProject.git
        
    Install python libraries by going to the project and execute: 
    
        pip install -r requirements.txt
        
    Before running server, make migrations
    
        python manage.py migrate cryptoApp
        
        python manage.py makemigrations cryptoApp
        
        python manage.py migrate cryptoApp
        
    To run server: 
    
        python manage.py runserver



Pip Freeze

certifi==2018.4.16
chardet==3.0.4
Django==2.0.5
idna==2.7
mysqlclient==1.3.13
numpy==1.15.0
pandas==0.23.4
protobuf==3.6.0
python-dateutil==2.7.3
pytz==2018.5
requests==2.19.1
six==1.11.0
urllib3==1.23


Conda list

# Name                    Version                   Build  Channel
ca-certificates           2018.03.07                    0  
certifi                   2018.4.16                py37_0  
chardet                   3.0.4                     <pip>
django                    2.0.5            py37hd476221_0  
idna                      2.7                       <pip>
libcxx                    4.0.1                h579ed51_0  
libcxxabi                 4.0.1                hebd6815_0  
libedit                   3.1.20170329         hb402a30_2  
libffi                    3.2.1                h475c297_4  
mysqlclient               1.3.13                    <pip>
ncurses                   6.1                  h0a44026_0  
numpy                     1.15.0                    <pip>
openssl                   1.0.2o               h1de35cc_1  
pandas                    0.23.4                    <pip>
pip                       10.0.1                   py37_0  
pip                       18.0                      <pip>
protobuf                  3.6.0                     <pip>
python                    3.7.0                hc167b69_0  
python-dateutil           2.7.3                     <pip>
pytz                      2018.5                   py37_0  
readline                  7.0                  hc1231fa_4  
requests                  2.19.1                    <pip>
setuptools                40.0.0                   py37_0  
six                       1.11.0                    <pip>
sqlite                    3.24.0               ha441bb4_0  
tk                        8.6.7                h35a86e2_3  
urllib3                   1.23                      <pip>
wheel                     0.31.1                   py37_0  
xz                        5.2.4                h1de35cc_4  
zlib                      1.2.11               hf3cbc9b_2  
